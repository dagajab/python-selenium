import pytest
from common import helpers, config
from selenium.webdriver import Remote as WebDriver


@pytest.fixture(scope="module")
def driver_instance() -> WebDriver:
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture()
def driver(driver_instance) -> WebDriver:
    driver_instance.get(config.WEB_SAMPLES_URL + "/users.html")
    return driver_instance


def test_creates_user(driver: WebDriver):
    # TODO Fill all the fields. Submit the form.
    # TODO Verify user was created
    pytest.fail("Not implemented! Remove this line.")


def test_fails_on_creating_user_with_no_data(driver: WebDriver):
    # TODO Submit empty form and verify all error messages
    pytest.fail("Not implemented! Remove this line.")


def test_fails_on_creating_user_with_invalid_email(driver: WebDriver):
    # TODO Fill all the fields. Make sure you enter invalid email
    # TODO Submit the form and verify invalid email message appears
    pytest.fail("Not implemented! Remove this line.")


def test_fails_on_creating_user_under_18(driver: WebDriver):
    # TODO Fill all the fields. Make sure you enter invalid age
    # TODO Submit the form and verify invalid age message appears
    pytest.fail("Not implemented! Remove this line.")
