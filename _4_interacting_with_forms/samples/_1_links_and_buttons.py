from time import sleep

import pytest
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.webdriver import Remote as WebDriver
from selenium.webdriver.common.by import By

from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance() -> WebDriver:
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance) -> WebDriver:
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample2.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_click_links(driver: WebDriver):
    # Find all links
    links = driver.find_elements(By.CSS_SELECTOR, "#anchors a")
    # Click all the links
    for i, link in enumerate(links):
        if link.is_enabled():
            print(f"Click #{i}: '{link.text}'")
            link.click()
            sleep(1)

    new_links_texts = []
    for link in links:
        new_links_texts.append(link.text)

    assert "Here we go!" in new_links_texts
    assert "Link 1 (clicked: true)" in new_links_texts
    assert "Link 2 (clicked: true)" in new_links_texts
    assert "Link 3 (clicked: true)" in new_links_texts


def test_click_buttons(driver: WebDriver):
    # Find all not disabled buttons
    buttons = driver.find_elements(By.CSS_SELECTOR, "#buttons button:not([disabled])")

    assert len(buttons) == 2

    # Click all the buttons
    for button in buttons:
        if button.is_enabled():
            button.click()
            sleep(1)


def test_click_disabled_button(driver: WebDriver):
    button = driver.find_element(By.CSS_SELECTOR, "#buttons button[disabled]")

    try:
        button.click()
    except ElementClickInterceptedException:
        print("Element not clickable!")
