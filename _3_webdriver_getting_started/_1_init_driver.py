import pytest
from selenium.webdriver import Remote as WebDriver


@pytest.fixture
def driver() -> WebDriver:
    # TODO Import globally required packages
    # TODO Init Firefox driver
    # TODO Maximize the window
    # TODO Implicit wait for elements to be found
    # TODO Close the driver after the test (yield)
    pytest.fail("Not implemented! Remove this line.")


# Inject driver from fixture as an argument into a test function
def test_get_sample1(driver: WebDriver):
    # TODO Import config from common
    # TODO Get sample1.html
    # TODO Assert page title
    # TODO Assert page url
    pytest.fail("Not implemented! Remove this line.")
