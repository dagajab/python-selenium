import pytest
from selenium.webdriver import Remote as WebDriver

from common import config, helpers


@pytest.fixture(scope="module")
def driver_instance() -> WebDriver:
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance) -> WebDriver:
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample1.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_1(driver: WebDriver):
    assert "Sample 1" == driver.title


def test_2(driver: WebDriver):
    assert "Sample 1" == driver.title


def test_3(driver: WebDriver):
    assert "Sample 1" == driver.title
