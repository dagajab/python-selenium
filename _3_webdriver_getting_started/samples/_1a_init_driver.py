from selenium.webdriver import Firefox
from selenium.webdriver.firefox.service import Service
from webdriver_manager.firefox import GeckoDriverManager

from common import config


def test():
    # Init firefox with Gecko driver and Firefox executables
    # driver = Firefox()
    driver = Firefox(service=Service(GeckoDriverManager().install()))

    # Implicit wait for element to be found
    driver.implicitly_wait(1)

    # Wait until page is loaded
    driver.get(config.WEB_SAMPLES_URL + "/sample1.html")

    # Print page title
    assert "Sample 1" == driver.title

    # Close the driver
    driver.close()
